if [[ ! -d .venv ]]; then
    python -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt
    deactivate
fi

if [[ ! -d mongodb ]]; then
    mkdir mongodb
fi

if [[ ! -d static/uploads ]]; then
    mkdir -r static/uploads
fi

source .venv/bin/activate
mongod -f mongodb.conf --fork
flask run
mongod -f mongodb.conf --shutdown
deactivate
