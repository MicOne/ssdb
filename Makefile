.PHONY: clean run

clean:
	rm -rf .venv mongodb __pycache__

run:
	./run.sh
