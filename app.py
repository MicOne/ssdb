from flask import Flask, request, redirect, flash, render_template, send_from_directory
from flask_login import LoginManager, UserMixin, login_required
from flask_login import current_user, login_user, logout_user
from werkzeug.security import generate_password_hash, check_password_hash

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from flask_wtf.csrf import CSRFProtect
from werkzeug.utils import secure_filename

from bson.objectid import ObjectId

import os

import pathlib

UPLOAD_FOLDER = 'static/uploads'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

from pymongo import MongoClient
CONNECTION_STRING =  "mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.2.2"
client = MongoClient(CONNECTION_STRING)
db = client['test']
users = db['users']
imgs = db['imgs']

from PIL import Image
import imagehash

import datetime

app = Flask(__name__)
app.debug = True
app.secret_key = os.urandom(12).hex()
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['TEMPLATES_AUTO_RELOAD'] = True

# casbin

from flask_authz import CasbinEnforcer
from casbin.persist.adapters import FileAdapter

app.config['CASBIN_MODEL'] = 'casbinmodel.conf'
app.config['CASBIN_OWNER_HEADERS'] = {'X-User', 'X-Group'}
app.config['CASBIN_USER_NAME_HEADERS'] = {'X-User'}
adapter = FileAdapter('policy.csv')
casbin_enforcer = CasbinEnforcer(app, adapter)

csrf = CSRFProtect(app)

login_manager = LoginManager()
login_manager.init_app(app)

def pwhash(password):
    return generate_password_hash(password, method='sha256')

from flask_login import UserMixin

@login_manager.user_loader
def load_user(user_id):
    """ get current_user """
    user_mixin = UserMixin()
    user_mixin.id = user_id
    return user_mixin

@app.route('/')
def heyo():
    """ default response """
    #return '''<img src="static/ok">'''
    return render_template('index.html', imgs=imgs, form=FlaskForm())
    if current_user.is_authenticated:
        return profile()
    return '''Heyo,
    <a href='/login'>log in</a> or
    <a href='/signup'>sign up</a>
    '''

@app.route('/login', methods=['GET', 'POST'])
def login():
    """ login user or show a form """
    if request.method == 'POST':
        #email = request.form['email']
        login = request.form['login']
        password = request.form['password']
        user = users.find_one({'login': login})
        if user and check_password_hash(user['password'], password):
            user_mixin = UserMixin()
            user_mixin.id = str(user['_id'])
            if current_user.is_authenticated:
                logout_user()
            login_user(user_mixin)
            return redirect('/')
        return '', 401
    return render_template('login.html', form=FlaskForm())
    return '''
        <form method="post">
            <p><input type=text name=login>
            <p><input type=password name=password>
            <p><input type=submit value="log in">
        </form>
    '''

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    """ signup user or show a form """
    if request.method == 'POST':
        #email = request.form['email']
        login = request.form['login']
        password = request.form['password']
        #if not re.match(r"[^@]+@[^@]+\.[^@]+", email) or len(password) < 8:
        #    return '', 403
        #User.signup(ver_code, login, generate_password_hash(password, method='sha256'))
        # testing: no unique constraint yet
        #if users.count_documents({'login': login}, limit = 1) == 0:
        if not users.find_one({'login': login}):
            users.insert_one({'login': login, 'password': pwhash(password)})
            return redirect('/login')
        return 'this user is already registered', 409
    return render_template('signup.html')

@app.route('/logout')
@login_required
def logout():
    """ logout user """
    logout_user()
    return redirect('/')

@app.route('/profile')
@login_required
def profile():
    return render_template('profile.html', imgs=imgs, user=users.find_one({'_id': ObjectId(current_user.id)})), 200

@app.route('/profile/set_avatar/<token>', methods=['POST'])
@login_required
def set_avatar(token):
    img = imgs.find_one({'$and': [{'token': token, 'private': False, 'owner': current_user.id}, {'$not': {'banned': True}}]})
    # with?
    if img:
        users.update_one({'_id': ObjectId(current_user.id)}, {'$set': {'avatar': img['token']}})
        return '', 200
    return '', 403

@app.route('/profile/remove/<token>', methods=['POST'])
@login_required
def remove(token):
    if current_user.id == imgs.find_one({'token': token})['owner']:
        imgs.delete_one({'token': token})
        return '', 200
    return '', 401

def allowed_file(filename):
    return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
@casbin_enforcer.enforcer
@login_required
def upload():
    if request.method == 'POST':
        file = request.files['file']
        password = request.form['password']
        private = bool(request.form.get('private'))
        if file and allowed_file(file.filename):
            # or just use the hash as a name
            #filename = secure_filename(file.filename)
            #filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            # or store as a number
            img = Image.open(file)
            token = str(imagehash.average_hash(Image.open(file))) # hash for comparision
            filename = token + pathlib.Path(file.filename).suffix
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            #if not os.path.isfile(filepath):
            if not imgs.find_one({'token': token}):
                img.save(filepath)
                imgs.insert_one({
                        'filename': filename,
                        'owner': current_user.id,
                        'token': token,
                        'password': pwhash(password),
                        'datetime': datetime.datetime.utcnow(),
                        'history': [{'owner': current_user.id, 'datetime': datetime.datetime.utcnow()}],
                        'liked_by': [], # user, date
                        'likes': 0, # aka cache
                        'comments': [], # user, date, text
                        'private': private
                                })
                return redirect('/'), 200
            return '', 403
        return '', 422
    return render_template('upload.html')

@app.route('/restore', methods=['GET', 'POST'])
@login_required
def claim():
    if request.method == 'POST':
        token = request.form['token']
        password = request.form['password']
        query = {'token': token}
        if check_password_hash(imgs.find_one(query)['password'], password):
            imgs.update_one(query, {
                '$set': {'owner': current_user.id},
                '$addToSet': {'history': {'owner': current_user.id, 'datetime': datetime.datetime.utcnow()}}
            })
            return '', 200
        return '', 403
    return '''
    <form method=post>
        <input type=text name=token>Token
        <input type=password name=password>Password
        <input type=submit value=restore>
    </form>
    '''

@app.route('/like/<token>', methods=['POST'])
@login_required
def like(token):
    if imgs.find_one({'token': token, 'liked_by.user': {'$ne': current_user.id}, 'private': False}):
        imgs.update_one({'token': token}, {
            "$addToSet": {"liked_by": {'user': current_user.id, 'datetime': datetime.datetime.utcnow()}},
            "$inc": {"likes": 1}
        })
        return str(imgs.find_one({'token': token})['likes']), 200
    else:
        return '', 400

@app.route('/ban/<token>', methods=['POST'])
@login_required
def ban(token):
    img.update_one({'token': token}, {'$set': {'banned': True}})
    if user := users.find_one({'avatar': token}) and users.find_one({'_id': ObjectId(current_user.id)})['role'] == 'moderator' :
        users.update_one({'_id': user['_id']}, {'$unset': {'avatar': ''}})

@app.route('/view/<token>')
def view_img(token):
    return str(imgs.find_one({'token': token,
        '$or': [
            {'owner': current_user.id} if current_user.is_authenticated else {},
            {'private': {'$ne': True}}
        ]
    }))

@app.route('/comment/<token>', methods=['POST'])
@login_required
def comment(token):
    imgs.update_one({'token': token}, {'$addToSet': {'comments': {'user': current_user.id, 'datetime': datetime.datetime.utcnow(), 'comment': clean_html(request.form['comment'])}}})
    return redirect(request.referrer), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0')
